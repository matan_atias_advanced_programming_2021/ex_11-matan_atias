#include "threads.h"
#include <iostream>


using std::cout;
using std::endl;
using std::cin;


#define I_LOVE_THREAD 1
#define CALC_PRIME 2
#define PRINT_PRIME 3
#define CALC_PRIME_AND_WRITE_TO_FILE 4


int main()
{
	int choice = -1;
	int begin = 0;
	int end = 0;
	int amountOfThreads = 0;
	std::string filePath;
	std::vector<int> primes;
	//declaring vars
	while (true)
	{
		while (choice < 0 || choice > 4)
		{
			cout << "1. Print \"I Love Threads\" using a thread" << endl;
			cout << "2. Calculate all prime numbers from a number to another number using a thread" << endl;
			cout << "3. Print all the prime numbers" << endl;
			cout << "4. Calculate all prime numbers from a number to another number and write it into a file using a thread" << endl;
			cout << "0. Exit" << endl;
			cout << "Your choice: ";
			cin >> choice;
			//getting choice from user
			if (choice < 0 || choice > 4)
			{
				cout << "Invalid choice!" << endl;
				system("PAUSE");
			}
			system("CLS");
		}
		switch (choice)
		{
			case I_LOVE_THREAD:
				call_I_Love_Threads();
				break;	
			case CALC_PRIME:
				cout << "Enter begin: ";
				cin >> begin;
				cout << "Enter end: ";
				cin >> end;
				//getting all info needed from user
				primes = callGetPrimes(begin, end);
				break;
			case PRINT_PRIME:
				printVector(primes);
				break;
			case CALC_PRIME_AND_WRITE_TO_FILE:
				cout << "Enter begin: ";
				cin >> begin;
				cout << "Enter end: ";
				cin >> end;
				cout << "Enter file path: ";
				cin >> filePath;
				cout << "Enter amount of threads you want to be created: ";
				cin >> amountOfThreads;
				//getting all info needed from user
				callWritePrimesMultipleThreads(begin, end, filePath, amountOfThreads);
				break;
			default:
				_exit(1);
		}
		choice = -1;
		//resetting vars
		system("PAUSE");
		system("CLS");
		//clearing the screen, starting new interaction(if choice wasn't 0)
	}	
	return 0;
}