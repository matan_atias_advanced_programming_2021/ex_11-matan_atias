#include <iostream>
#include <thread>
#include <iostream>
#include <fstream>
#include "threads.h"


using std::cout;
using std::endl;
using std::ofstream;


/*
	Function prints "I Love Threads"
	Input:
		None
	Output:
		None
*/
void I_Love_Threads()
{
	cout << "I Love Threads" << endl;
}


/*
	Function calls I_LoveThreads function using a thread
	Input:
		None
	Output:
		None
*/
void call_I_Love_Threads()
{
	std::thread t1(I_Love_Threads);
	//calling I_Love_Threads function using a thread
	t1.join();
	//waiting for the thread to finish
}


/*
	Function gets a vector and prints all its values
	Input:
		vector
	Output:
		None
*/
void printVector(std::vector<int> primes)
{
	unsigned int i = 0;
	//declaring vars
	for (i = 0; i < primes.size(); i++)
	{
		cout << primes[i] << endl;
	}
}


/*
	Function puts all prime numbers from begin to end in a vector
	Input:
		begin, end, vector for all the prime numbers
	Output:
		None
*/
void getPrimes(int begin, int end, std::vector<int>& primes)
{
	int i = begin;
	int j = 0;
	bool flag = true;
	//declaring vars
	if (begin < 2)
	{
		begin = 3;
	}
	//negative numbers / 1 / 2 cant be prime numbers
	for (i = begin; i < end; i++)
	{
		//if a number can be divided by 2, it can't be a prime number
		if (i % 2 != 0)
		{
			for (j = 2; j < i / 2 && flag; j++)
			{
				if (i % j == 0)
				{
					flag = false;
				}
			}
			if (flag)
			{
				primes.push_back(i);
			}
			flag = true;
			//resetting flag
		}
	}
}

/*
	Function calls getPrimes function using a thread
	Input:
		begin, end
	Output:
		vector<int>
*/
std::vector<int> callGetPrimes(int begin, int end)
{
	std::vector<int> primes;
	//declaring vars
	std::thread t1(getPrimes, begin, end, ref(primes));
	//calling getPrimes from a thread and sending it begin, end, and a refrence to a vector
	t1.join();
	//waiting for the thread to finish
	return primes;
}


/*
	Function gets a range of numbers and a refrence to a file and writes all the prime numbers in the range to the file
	Input:
		begin, end, file
	Output:
		None
*/
void writePrimesToFile(int begin, int end, std::ofstream& file)
{
	int i = begin;
	int j = 0;
	bool flag = true;
	//declaring vars
	if (begin < 2)
	{
		begin = 3;
	}
	//negative numbers / 1 / 2 cant be prime numbers
	for (i = begin; i < end; i++)
	{
		//if a number can be divided by 2, it can't be a prime number
		if (i % 2 != 0)
		{
			for (j = 2; j < i / 2 && flag; j++)
			{
				if (i % j == 0)
				{
					flag = false;
				}
			}
			if (flag)
			{
				file << i << ", ";
			}
			flag = true;
			//resetting flags
		}
	}
}


/*
	Function gets a range of numbers, divides it to N small ranges, creates N threads and calling writePrimeToFile from each thread
	Input:
		begin, end, filepath, N
	Output:
		None
*/
void callWritePrimesMultipleThreads(int begin, int end, std::string filePath, int N)
{
	int i = 0;
	ofstream myFile;
	int count = (end - begin) / N;
	//declaring vars
	myFile.open(filePath);
	if (myFile.is_open())
	{
		for (i = 1; i <= N; i++)
		{
			if (i == N)
			{
				std::thread t1(writePrimesToFile, begin + (count * (i - 1)), end, ref(myFile));
				//calling writePrimesToFile from a thread and sending it begin, end, and a refrence to a file
				t1.join();
				//waiting for the thread to finish
			}
			else
			{
				std::thread t1(writePrimesToFile, begin + (count * (i - 1)), begin + (count * i), ref(myFile));
				//calling writePrimesToFile from a thread and sending it begin, end, and a refrence to a file
				t1.join();
				//waiting for the thread to finish
			}
		}
	}
	else
	{
		cout << "Error opening file" << endl;
	}
}